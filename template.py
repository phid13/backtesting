# imports
import datetime
import yfinance as yf
import pyplot as pt
import math
import functions as fn
import maths
import sort_dates as dt
import numpy as np
import pandas as pd

def yfinance(stocks, start_date, end_date, offset):
    if offset != 0:
        year = int(start_date[:4])
        month = int(start_date[5:7])
        day = int(start_date[8:10])
        start_date = datetime.date(year, month, day)
        start_date -= datetime.timedelta(days=offset)
        year = str(start_date.year)
        month = start_date.month
        day = start_date.day
        if month < 10:
            month = "0" + str(month)
        else:
            month = str(month)
        if day < 10:
            day = "0" + str(day)
        else:
            day = str(day)
        start_date = year + "-" + month + "-" + day
        x = input(start_date)

    sectors = []
    for stock in stocks:
        data = yf.download(stock, start_date, end_date).reset_index()
        data['Date'] = data['Date'].apply(lambda x: datetime.datetime.strftime(x, '%Y-%m-%d'))
        cprcs = data['Close'].values
        sectors.append(data)
    return sectors

def Main():
    start_date = '2007-01-01'
    end_date = '2021-12-31'
    offset = 280

    stocks = ['SPY', 'Other']
    sectors = yfinance(stocks, start_date, end_date, offset)
    # read stocks
    stock_1 = pt.read("stock_1.csv")
    stock_2 = pt.read("stock_2.csv")
    stock_3 = pt.read("stock_3.csv")

    spy = pt.read("SPY.csv")

    # make list of all csvs
    sectors = [stock_1, stock_2, stock_3, spy] + sectors
    
    # parse csvs into useful data
    sd = dt.sort_dates(sectors)
    sd.set_start(start_date, offset)
    sd.set_end(end_date)
    sd.sort_dates()

    dates = sd.getDates()
    cprcs = sd.getCprcs()

    spy_cls = cprcs[-1]
    cprcs = np.delete(cprcs, -1, 0)

    spy_bandh = maths.BandH(spy_cls, 280)

    # do strategy

    # plot
    array = [strategy_1, strategy_2, strategy_3, spy_bandh]
    names = ['dates', 'strategy 1', 'strategy 2', 'strategy 3', 'spy buy and hold']
    title = 'title'

    data = pt.Data(dates, array, names, title)
    data.plot()

Main()
