import datetime
import numpy as np
import pandas as pd

def remove_comma(string):
    newvalue = ""
    for i in range(0, len(str(string))):
        if str(str(string)[i]) != ",":
            newvalue = newvalue + str(str(string)[i])
    return float(newvalue)

class sort_dates:

    def __init__(self, dfs):
        # Change stocks from DataFrames to numpy_arrays
        # Within stocks[i] represents the ith stock
        # Stocks[i][j] represents the jth property in the following order:
        # Date, Open, Low, High, Close, Volume 
        # Stocks[i][j][k] represents an indivudual value

        self.stocks = []
        self.start_date_exists = False
        self.end_date_exists = False
        for df in dfs:
            try_worked = True
            Date, Open, Low, High, Close, Volume = [], [], [], [], [], []
            try:
                date = datetime.date(int(df["Date"][0][:4]), int(df["Date"][0][5:][:2]), int(df["Date"][0][8:][:2])) 
                for i in range(0, len(df["Date"])):
                    date = datetime.date(int(df["Date"][i][:4]), int(df["Date"][i][5:][:2]), int(df["Date"][i][8:][:2])) 
                    Date.append(date)
            except:
                try_worked = False
                try:
                    months = ['Month 0', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    for i in range(0, len(df["Date"])):
                        day = int(df["Date"][i][4:][:2])
                        month = int(months.index(df["Date"][i][:3]))
                        year = int(df["Date"][i][8:][:4])
                        date = datetime.date(year, month, day)
                        Date.append(date)
                        Close.append(remove_comma(df["Price"][i]))
                        # Volume.append(remove_comma(df["Vol."][i]))
                        Open.append(remove_comma(df["Open"][i]))
                        Low.append(remove_comma(df["Low"][i]))
                        High.append(remove_comma(df["High"][i]))
                except:
                    for i in range(0, len(df["Date"])):
                        day = int(df["Date"][i][3:][:2])
                        month = int(df["Date"][i][:2])
                        year = int(df["Date"][i][6:][:4])
                        date = datetime.date(year, month, day)
                        Date.append(date)
                        Close.append(remove_comma(df["Price"][i]))
                        # Volume.append(remove_comma(df["Vol."][i]))
                        Open.append(remove_comma(df["Open"][i]))
                        Low.append(remove_comma(df["Low"][i]))
                        High.append(remove_comma(df["High"][i]))

                Date = Date[::-1]
                Close = Close[::-1]
                # Volume = Volume[::-1]
                Open = Open[::-1]
                Low = Low[::-1]
                High = High[::-1]
            npDate = np.asarray(Date)
            if try_worked:
                npClose = df["Close"].to_numpy()
                npOpen = df["Open"].to_numpy()
                npHigh = df["Low"].to_numpy()
                npLow = df["High"].to_numpy()
            else:
                npClose = np.asarray(Close)
                npOpen = np.asarray(Open)
                npHigh = np.asarray(High)
                npLow = np.asarray(Low)
            np_stocks = np.stack((npOpen, npLow, npHigh, npClose)) 
            np_stocks = np.transpose(np_stocks)
            self.stocks.append(pd.DataFrame(data=np_stocks, index=npDate))

    # for the dates, yyyy, mm, dd
    def set_start(self, start_date, offset=0):
        year = int(start_date[:4])
        month = int(start_date[5:7])
        day = int(start_date[8:10])
        start_date = datetime.date(year, month, day) 
        start_date -= datetime.timedelta(days=offset)
        self.start_date_exists = True
        self.start_date = start_date

    def set_end(self, end_date):
        year = int(end_date[:4])
        month = int(end_date[5:7])
        day = int(end_date[8:10])
        end_date = datetime.date(year, month, day) 
        self.end_date_exists = True
        self.end_date = end_date

    def sort_dates(self, drange="max"):
        # Get start date and end date
        starts = []
        ends = []
        for stock in self.stocks:
            starts.append(stock.index[0])
            ends.append(stock.index[-1])
        start_date = starts[0]
        end_date = ends[0]
        for i in starts:
            if drange == "max":
                if i < start_date:
                    start_date = i
            else:
                if i > start_date:
                    start_date = i
        for i in ends:
            if drange == "max":
                if i > end_date:
                    end_date = i
            else:
                if i < end_date:
                    end_date = i
        if self.start_date_exists == True:
            start_date = self.start_date
        if self.end_date_exists == True:
            end_date = self.end_date
        # Get full list of dates within range
        Dates = pd.date_range(start=start_date, end=end_date)
        dates = []
        for i in range(0, len(Dates)):
            dates.append(Dates[i].date())
        # For each stock, add missing dates with null values
        for i in range(0, len(self.stocks)):
            self.stocks[i] = self.stocks[i].reindex(dates)
        array_stocks = []
        array_cprcs = []
        for stock in self.stocks:
            cur_stock = np.transpose(stock.to_numpy())
            array_stocks.append(cur_stock)
            array_cprcs.append(cur_stock[3])
        self.stocks = np.array(array_stocks)
        self.cprcs = np.array(array_cprcs)
        self.dates = np.array(dates)

    def getDates(self):
        return self.dates

    # stocks is open, low, high, close
    def getStocks(self):
        return self.stocks

    def getCprcs(self):
        return self.cprcs

# Method that takes a list of datetimes in format yyyy-mm-dd hh:mm:ss-04:00
# and a list of 15 minute values (eg close price)
# The returns are the following:
# a list of dates
# a list containing a list of the values for each day
def getDays(td, close):
    ##########################################################################################################
    # int(td[i][:4])                                                                  = year
    # int(td[i][2:][:2])                                                              = last 2 digits of year
    # int(td[i][5:][:2])                                                              = month
    # int(td[i][8:][:2])                                                              = day
    # td[i][:10]                                                                      = curDay
    # int(td[i][11:][:2])                                                             = hour
    # int(td[0][14:][:2])                                                             = minute (every 15 mins)
    # datetime.date(int(td[i][:4]), int(td[i][5:][:2]), int(td[i][8:][:2])).weekday() = day of the week
    ##########################################################################################################
    numbers = []
    curDay = td[0][:10]
    dates = []
    dailyClose = []
    bankHoliday = False
    for i in range(0, len(td)):
        if curDay == td[i][:10]:    # if (still same day)
            numbers.append(close[i])
        else:                       # if (not still same day)
            if datetime.date(int(td[i][:4]), int(td[i][5:][:2]), int(td[i][8:][:2])).weekday() == 0:                   # if monday
                if bankHoliday == False:    # If it's not a bank holiday, treat monday as tuesday-thursday
                    if (int(td[i][11:][:2]) < 16) | (
                            (int(td[i][11:][:2]) == 16) & (int(td[i][14:][:2]) == 0)):  # if time <= 16:00
                        numbers.append(close[i])
                    else:  # if time > 16:30
                        if (0 <= int(td[i][5:][:2]) <= 12) & (int(td[i][2:][:2]) != 1):  # if within date range then add
                            dailyClose.append(numbers)  # start new day
                            dates.append((datetime.date(int(td[i][:4]), int(td[i][5:][:2]),
                                                        int(td[i][8:][:2])) + datetime.timedelta(days=1)).strftime(
                                '%Y-%m-%d'))
                        curDay = td[i][:10]
                        numbers = []
                        numbers.append(close[i])
                else:     # if it is a bank holiday treat it as a sunday
                    if (0 <= int(td[i][5:][:2]) <= 12) & (int(td[i][2:][:2]) != 1):  # if within date range then add
                        dailyClose.append(numbers)  # start new day
                        dates.append((datetime.date(int(td[i][:4]), int(td[i][5:][:2]),
                                                    int(td[i][8:][:2])) + datetime.timedelta(days=1)).strftime(
                            '%Y-%m-%d'))
                    curDay = td[i][:10]
                    numbers = []
                    numbers.append(close[i])
            elif 1 <= datetime.date(int(td[i][:4]), int(td[i][5:][:2]), int(td[i][8:][:2])).weekday() <= 3:            # if (tuesday to thursday)
                bankHoliday = True
                if (int(td[i][11:][:2]) < 16) | ((int(td[i][11:][:2]) == 16) & (int(td[i][14:][:2]) == 0)):           # if time <= 16:00
                    numbers.append(close[i])
                else:                               # if time > 16:30
                    if (0 <= int(td[i][5:][:2]) <= 12) & (int(td[i][2:][:2]) != 1):  # if within date range then add
                        dailyClose.append(numbers)   # start new day
                        dates.append((datetime.date(int(td[i][:4]), int(td[i][5:][:2]), int(td[i][8:][:2])) + datetime.timedelta(days=1)).strftime('%Y-%m-%d'))
                    curDay = td[i][:10]
                    numbers = []
                    numbers.append(close[i])
            elif datetime.date(int(td[i][:4]), int(td[i][5:][:2]), int(td[i][8:][:2])).weekday() == 4:    # if (friday)
                numbers.append(close[i])
            elif datetime.date(int(td[i][:4]), int(td[i][5:][:2]), int(td[i][8:][:2])).weekday() == 6:    # if (sunday)
                bankHoliday = False
                if (0 <= int(td[i][5:][:2]) <= 12) & (int(td[i][2:][:2]) != 1):  # if within date range then add
                    dailyClose.append(numbers)  # start new day
                    dates.append((datetime.date(int(td[i][:4]), int(td[i][5:][:2]), int(td[i][8:][:2])) + datetime.timedelta(days=1)).strftime('%Y-%m-%d'))
                curDay = td[i][:10]
                numbers = []
                numbers.append(close[i])
            else:
                error(td, i)
                x = input("press 'enter' to continue")
    return dailyClose, dates
