import pandas as pd
import maths
import matplotlib.pyplot as plt
import glob

class Data:

    def __init__(self, dates, array, names, title):
        self.dates = dates
        self.array = array
        self.names = names
        self.title = title

    def set_array(self, array):
        self.array = array

    def set_names(self, names):
        self.names = names

    def plot(self, bold=0, show=True, save_name=None):
        array = [self.dates] + self.array 
        array_t = self.transpose(array) 
        df = pd.DataFrame(array_t, columns=self.names)
        bold += 1
        fig, ax = plt.subplots()
        for i in range(1, bold):
            ax.plot(df['dates'], df[self.names[i]], label=self.names[i], linewidth=3)
        l = len(self.names) - 1
        for i in range(bold, len(self.names)):
            ax.plot(df['dates'], df[self.names[i]], label=self.names[i], linewidth=1)

        plt.ylabel("Equities")
        plt.legend()
        plt.xticks(rotation=90)
        plt.title(self.title)
        plt.grid(axis='y')
        if show:
            plt.show()
        if save_name is not None:
            plt.savefig(save_name)

    def write(self):
        array = [self.dates] + self.values
        array_t = transpose(array)
        df = pd.DataFrame(array_t, columns=self.names)
        df.to_csv(self.title, index=False)

    def print_metrics(self, caar=False, mdd=False, mdd_duration=False, cum_ret=False, mdd_dates=False, aar=False):
        for i, results in enumerate(self.array):
            print(self.names[i+1])
            if caar:
                print("AACR: " + self.round(maths.CAAR(results, self.dates)) + "%")
            if mdd:
                print("MDD: " + self.round(maths.MDD(results)) + "%")
            if mdd_duration:
                print("MDD Duration: " + str(maths.MDDLength(results, self.dates)) + " days")
            if cum_ret:
                print("Cumulative returns: " + str((results[-1] - 1) * 100) + "%")
            if mdd_dates:
                print("MDD Dates: " + maths.MDDDates(results, self.dates))
            if aar:
                print("AAR: " + self.round(maths.AAR(results, self.dates)) + "%") 
            print()

    def csv_metrics(self, caar=False, mdd=False, mdd_duration=False, cum_ret=False, mdd_dates=False, aar=False, yearly_return=False):
        columns = [''] + self.names[1:]
        data = []
        if caar:
            data.append(["AACR"] + [(self.round(maths.CAAR(results, self.dates)) + "%") for results in self.array])
        if mdd:
            data.append(["MDD"] + [(self.round(maths.MDD(results)) + "%") for results in self.array])
        if mdd_duration:
            data.append(["MDD Duration"] + [(str(maths.MDDLength(results, self.dates)) + " days") for results in self.array])
        if cum_ret:
            data.append(["Cumulative returns"] + [(str(round((results[-1] - 1) * 100)) + "%") for results in self.array])
        if mdd_dates:
            data.append(["MDD Dates"] + [maths.MDDDates(results, self.dates) for results in self.array])
        if aar:
            data.append(["AAR"] + [(self.round(maths.AAR(results, self.dates)) + "%") for results in self.array]) 
        if yearly_return:
            data.append(["Annual Returns"] + ['' for i in range(len(self.array))])
            data += self.annual_returns()
            
        df = pd.DataFrame(data=data, columns=columns)
        title = self.title + ".csv"
        df.to_csv(title, index=False)

    def annual_returns(self):
        dates = self.dates
        array = self.array
        annual_returns = []
        year = dates[0].year
        s_day = 0
        for i in range(len(dates)):
            if ((i == len(dates) - 1) | (dates[i].year != year)):
                annual_returns.append([year] + [results[i-1] / results[s_day] for results in array])
                year = dates[i].year
                s_day = i
        return annual_returns

    def transpose(self, array):
        array_t = []
        for i in range(0, len(array[0])):
            new_row = []
            for j in range(0, len(array)):
                new_row.append(array[j][i])
            array_t.append(new_row)
        return array_t
    
    def round(self, val):
        string = '%s' % float('%.3g' % val)
        return string

def read(title):
    return pd.read_csv(title)

def read_folder(path):
    path = path + "/*.csv"
    folder = glob.glob(path)
    files = []
    for file in folder:
        csv = pd.read_csv(file)
        files.append(csv)
    return files
