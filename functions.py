import maths
import pandas as pd
import math
import numpy as np

# go long a stock when above a moving average
def long(cprc, days=28, leverage=1):
    np_cls = np.array(cprc)
    np_avgs = np.zeros(len(cprc))
    for i in range(len(cprc)-days):
        np_avgs[i + days] = np.nanmean(np_cls[i:i+days])
    pd_cls = pd.Series(np_cls).ffill() # fill nan values with prev non-nan
    np_cls = np.array(pd_cls)
    log_returns = np.log(np.divide(np_cls[1:], np_cls[:-1])) * leverage
    log_returns = np.append([0], log_returns)
    for i in range(1, len(log_returns)):
        if np_cls[i-1] < np_avgs[i]:
            log_returns[i] = 0
    log_returns = np.nan_to_num(log_returns)
    log_returns = log_returns[days:]
    log_returns = np.append(np.zeros(days), log_returns)
    cum_sum = np.cumsum(log_returns)
    returns = np.exp(cum_sum)
    return returns

def short(cprc, days=28, leverage=-1):
    np_cls = np.array(cprc)
    np_avgs = np.zeros(len(cprc))
    for i in range(len(cprc)-days):
        np_avgs[i + days] = np.nanmean(np_cls[i:i+days])
    pd_cls = pd.Series(np_cls).ffill() # fill nan values with prev non-nan
    np_cls = np.array(pd_cls)
    log_returns = np.log(np.divide(np_cls[1:], np_cls[:-1])) * leverage
    log_returns = np.append([0], log_returns)
    for i in range(1, len(log_returns)):
        if np_cls[i-1] > np_avgs[i]:
            log_returns[i] = 0
    log_returns = np.nan_to_num(log_returns)
    log_returns = log_returns[days:]
    log_returns = np.append(np.zeros(days), log_returns)
    cum_sum = np.cumsum(log_returns)
    returns = np.exp(cum_sum)
    return returns
