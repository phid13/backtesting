def year_long(cprc, avg, leverage=1):
    np_cls = np.array(cprc)
    np_avgs = np.array(avg)
    pd_cls = pd.Series(np_cls).ffill() # fill nan values with prev non-nan
    np_cls = np.array(pd_cls)
    log_returns = np.log(np.divide(np_cls[1:], np_cls[:-1])) * leverage
    log_returns = np.append([0], log_returns)
    for i in range(1, len(log_returns)):
        if np_cls[i-1] < np_avgs[i]:
            log_returns[i] = 0
    log_returns = np.nan_to_num(log_returns)
    cum_sum = np.cumsum(log_returns)
    returns = np.exp(cum_sum)
    return returns

def bandh(all_cprcs, dates):
    year = 2015
    prev_i = 0
    all_returns = np.array([1])
    for i in range(len(dates)):
        if (i == len(dates) - 1) | (dates[i].year != year):
            cprcs = all_cprcs[:,prev_i:i]
            prev_i = i
            returns = np.ones_like(cprcs[0]) * all_returns[-1]
            for stock in cprcs:
                returns *= maths.BandH(stock, leverage=0.33)
            all_returns = np.concatenate((all_returns, returns))
            year = dates[i].year
    all_returns /= all_returns[280]
    return all_returns

def long(all_cprcs, dates):
    year = 2015
    prev_i = 0
    all_returns = np.array([1])
    all_avgs = np.zeros_like(all_cprcs)
    for i in range(len(all_cprcs)):
        cprc = all_cprcs[i]
        pd_cls = pd.Series(cprc).ffill() # fill nan values with prev non-nan
        np_cls = np.array(pd_cls)
        all_avgs[i] = maths.MovingAverage(np_cls, 280)
        all_cprcs[i] = np_cls
    for i in range(len(dates)):
        if (i == len(dates) - 1) | (dates[i].year != year):
            cprcs = all_cprcs[:,prev_i:i]
            avgs = all_avgs[:,prev_i:i]
            prev_i = i
            returns = np.ones_like(cprcs[0]) * all_returns[-1]
            for j in range(len(cprcs)):
                if j == 0: # for iuit
                    returns *= year_long(cprcs[j], avgs[j], leverage=0.25)
                else:
                    returns *= maths.BandH(cprcs[j], leverage=0.25)
            all_returns = np.concatenate((all_returns, returns))
            year = dates[i].year
    all_returns /= all_returns[280]
    return all_returns
