import math
import pandas as pd
import datetime
import numpy as np

def MovingAverage(numbers, days=20):
    numbers = np.array(numbers)
    avgs = np.zeros(len(numbers))
    for i in range(len(numbers) - days):
        avgs[i + days] = np.nanmean(numbers[i:i+days])
    return avgs

def MovingStdDev(numbers, days=20):
    numbers = np.array(numbers)
    std_devs = np.zeros(len(numbers))
    for i in range(len(numbers) - days):
        std_devs[i + days] = np.nanstd(numbers[i:i+days])
    return std_devs

def CAAR(equities, dates):
    i = 0
    while equities[i] == equities[0]:
        i += 1
    s_date = dates[i]
    e_date = dates[-1]
    days = (e_date - s_date).days
    years = days / 365.25
    CAAR = (((equities[len(equities) - 1]) ** (1/years)) * 100) - 100
    return CAAR

def AAR(equities, dates):
    i = 0
    while equities[i] == equities[0]:
        i += 1
    s_date = dates[i]
    e_date = dates[-1]
    days = (e_date - s_date).days
    years = days / 365.25
    aar = ((equities[-1] - 1) * 100) / years
    return aar

def MDD(equities):
    peak = -99999
    MDD = 0
    for i in range(0, len(equities)):
        if equities[i] > peak:
            peak = equities[i]
        DD = 100 * (peak - equities[i]) / peak
        if DD > MDD:
            MDD = DD
    return MDD

def MDDratio(AAR, MDD):
	return AAR/MDD

def MDDLength(equities, dates):
    peak = -99999
    MDD = 0
    start_date = dates[0]
    new_max = False
    length = 0
    for i in range(0, len(equities)):
        if equities[i] > peak:
            if new_max:
                length = (dates[i] - start_date).days
            new_max = False
            peak = equities[i]
            start_date = dates[i]
        DD = 100 * (peak - equities[i]) / peak
        if DD > MDD:
            MDD = DD
            new_max = True
    return length

def MDDDates(equities, dates):
    peak = -99999
    MDD = 0
    start_date = dates[0]
    new_max = False
    for i in range(0, len(equities)):
        if equities[i] > peak:
            if new_max:
                end = dates[i].strftime("%d/%m/%Y")
                actual_start = start_date.strftime("%d/%m/%Y")
            new_max = False
            peak = equities[i]
            start_date = dates[i]
        DD = 100 * (peak - equities[i]) / peak
        if DD > MDD:
            MDD = DD
            new_max = True
    return (actual_start + " to " + end)

def BandH(cls, start=0, leverage=1):
    pd_cls = pd.Series(cls).ffill() # fill nan values with prev non-nan
    np_cls = np.array(pd_cls)
    np_divide = np.divide(np_cls[1:], np_cls[:-1])
    np_leverage = ((np_divide - 1) * leverage + 1)
    log_returns = np.log(np_leverage)
    log_returns = np.append([0], log_returns)
    log_returns = np.nan_to_num(log_returns)
    cum_sum = np.cumsum(log_returns)
    cum_sum = cum_sum - cum_sum[start]
    return np.exp(cum_sum)

def PriceDiff(prices, days=20):
    returns = []
    most_recent = prices[0]
    for i in range(len(prices)):
        if math.isnan(prices[i]):
            if len(returns) == 0:
                returns.append(0)
            else:
                returns.append(returns[-1])
        elif i < days:
            returns.append(prices[i] / prices[0])
        elif math.isnan(prices[i-days]):
            returns.append(prices[i] / most_recent)
        else:
            most_recent = prices[i-days]
            returns.append(prices[i] / most_recent)
    return returns







